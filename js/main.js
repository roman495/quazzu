$(document).ready(function () {
    $('.sidenav').sidenav()

    $('.carousel.carousel-slider').carousel({
        duration: 100,
        fullWidth: true,
        indicators: true
    })

    $('.modal').modal()

})

document.addEventListener('DOMContentLoaded', function() {
    M.Tabs.init(document.querySelector('.tabs'))
    M.FormSelect.init(document.querySelectorAll('select'));
})